# Bereitstellung eine FTP-Servers zum Speichern der Bilddaten

Zur Analyse der Bilddaten müssen diese auf einem zentralen Speicher abgelegt werden. Hierfür eignet sich ein FTP-Server aufgrund der großen Kompatibilität für unterschiedliche Plattformen.
Für die Bereitstellung des Servers wurde ein Docker-compose-Stack angelegt, der ein einfaches Starten des Servers auf unterschiedlichen Servern ermöglicht. 

## Voraussetzungen
Vorausgesetzt wird ein Sever, der an das Netzwerk angebunden ist und Docker installiert hat. Falls Docker nicht installiert wurde, kann dies mit dieser [Anleitung](https://docs.docker.com/get-docker/) geschehen. Wichtig ist, dass auch Docker-Compose mitinstalluiert wird, da es nicht bestandteil der Standardinstallation ist. Die [Anleitung](https://docs.docker.com/compose/install/) ist ebenfalls in der Dokumentation zu finden.

## Konfiguration
Die Konfiguration des Servers erfolgt über die Datei `docker-compose.yaml`, die im Ordner `docker-compose` gespeichert ist.

Hier können Benutzername und Passwort verändert werden. Ebenfalls kann der Pfad des ftp-Ordners angepasst werden. 

```yaml
version: '3.3'
services:
    proftpd:
        network_mode: host
        environment:
            - FTP_USERNAME=ftpuser
            - FTP_PASSWORD=123456
        volumes:
            - ../ftp-folder:/home/$FTP_USERNAME
        image: hauptmedia/proftpd
```

## Ausführung

Der Stack kann nun entwerder über eine grafische Benutzeroberfläche (z.B. nach Installation von [Portainer](https://docs.portainer.io/start/install-ce/server/docker/linux)) oder über die Kommandozeile gestartet werden.
Für letztere Möglichkeit wird das Kommando:
`docker compose up` bzw. `docker compose up -d` (starten im Hintergrund) genutzt werden.


## Nutzung

Im Verzeichnis `ftp-server` wird ein Ordner mit dem Namen `ftp-folder` angelegt. Darin befindet sich für jeden FTP-User ein Verzeichnis.