#include "esp_camera.h"
#include <WiFi.h>
#include <WiFiClient.h> 
#include <ESP32_FTPClient.h>
#include "string.h"
#include "time.h"
#include <EEPROM.h>
#include <ESPUI.h>
#include <ESPmDNS.h>

//Camer Settings
#define CAMERA_MODEL_AI_THINKER // Has PSRAM

#include "camera_pins.h"

struct {
  char ssid [32];
  char password [63];
  //String ssid = "fuchsbau";
  //String password = "50583356519873956870";
} wifi_credentials;

struct {
	uint32_t interval = 30000;
} cam_settings;

struct {
	char ip [16];
	char username [32];
	char password[63];
	char path [32];
  //String ip;
  //String username;
  //String password;
  //String path;
} ftp_config;

const char* ssid = "fuchsbau";
const char* password = "50583356519873956870";
//const char * ssid = "Show_Network";
//const char* password = "grandma2";

//char ftp_server[] = "192.168.178.59";
char ftp_server[] = "192.168.178.81";
char ftp_user[]   = "ftpuser";
char ftp_pass[]   = "123456";

ESP32_FTPClient ftp (ftp_server,ftp_user,ftp_pass, 5000, 2);

const char* ntpServer = "pool.ntp.org";
const long  gmtOffset_sec = 3600;
const int   daylightOffset_sec = 3600;

int imgCnt = 0;
String imgNameBase = "espCamRgbImg_";

unsigned long lastImageTaken = cam_settings.interval;

//Settings UI
#define SLOW_BOOT 0
#define HOSTNAME "ESP-Cam"
#define FORCE_USE_HOTSPOT 0
#define EEPROM_PARAM_MAX_LENGTH 128
#define EEPROM_PARAM_WIFI_SSID 0 
#define EEPROM_PARAM_WIFI_PASSWORT 1*EEPROM_PARAM_MAX_LENGTH 
#define EEPROM_PARAM_FTP_SERVER_IP 2*EEPROM_PARAM_MAX_LENGTH 
#define EEPROM_PARAM_FTP_BENUTZERNAME 3*EEPROM_PARAM_MAX_LENGTH 
#define EEPROM_PARAM_FTP_PASSWORT 4*EEPROM_PARAM_MAX_LENGTH 
#define EEPROM_PARAM_FTP_PFAD 5*EEPROM_PARAM_MAX_LENGTH 

//Function Prototypes
void connectWifi();
void setUpUI();
void reloadFTP(Control *sender, int type);
void enterWifiDetailsCallback(Control *sender, int type);
void enterFTPDetailsCallback(Control *sender, int type);
void textCallback(Control *sender, int type);
void intervalCallback(Control *sender, int type);

//UI handles
	//wifi
uint16_t wifi_ssid_text, wifi_pass_text, styleSaveWIFIbutton, grouplabelWifi;
	//FTP
uint16_t styleLoadFTPbutton, styleSaveFTPbutton, server_IP_text, benutzername_text, passwort_text, pfad_text;

uint16_t pic_interval;

/* volatile bool updates = false; */

void startCameraServer();
camera_fb_t *getImg();

// This is the main function which builds our GUI
void setUpUI() {
	//Turn off verbose debugging
	ESPUI.setVerbosity(Verbosity::Quiet);

	/*
	 * Tab: FTP
	 * FTP Konfigurations TAB
	 *-----------------------------------------------------------------------------------------------------------*/
	auto FTP = ESPUI.addControl(Tab, "", "FTP-Konfiguration");
	server_IP_text = ESPUI.addControl(Text, "FTP-Konfiguration", "Server IP", Alizarin, FTP, textCallback);
	ESPUI.setPanelStyle(server_IP_text, "background: linear-gradient(90deg, rgba(255,58,180,1) 0%, rgba(0,29,29,1) 50%, rgba(100,176,69,1) 100%); border-bottom: #555;");
	benutzername_text = ESPUI.addControl(Text, "Benutzername", "Benutzername", Alizarin, server_IP_text, textCallback);
	passwort_text = ESPUI.addControl(Text, "Passwort", "Passwort", Alizarin, server_IP_text, textCallback);
	pfad_text = ESPUI.addControl(Text, "Pfad", "Pfad", Alizarin, server_IP_text, textCallback);

	
	styleLoadFTPbutton = ESPUI.addControl(Button, "Load", "Load", Alizarin, server_IP_text, reloadFTP);
	ESPUI.setElementStyle(styleLoadFTPbutton, "border-radius: 2em; border: 3px solid black; width: 30%; background-color: #ff00ff;");

	styleSaveFTPbutton = ESPUI.addControl(Button, "Save", "Save", Alizarin, server_IP_text, enterFTPDetailsCallback);
	ESPUI.setElementStyle(styleSaveFTPbutton, "border-radius: 2em; border: 3px solid black; width: 30%; background-color: #ff00ff;");


	/*
	 * Tab: WiFi Konfiguration
	 * You use this tab to enter the SSID and password of a wifi network to autoconnect to.
	 *-----------------------------------------------------------------------------------------------------------*/
	auto wifitab = ESPUI.addControl(Tab, "", "WiFi Konfiguration");
	
	//auto grouplabelTEST = ESPUI.addControl(Label, "WiFi","", Sunflower, wifitab);
	wifi_ssid_text = ESPUI.addControl(Text, "WiFi","SSID", Sunflower, wifitab, textCallback);
	ESPUI.setPanelStyle(wifi_ssid_text, "background: linear-gradient(90deg, rgba(131,58,180,1) 0%, rgba(253,29,29,1) 50%, rgba(252,176,69,1) 100%); border-bottom: #555;");
	//Note that adding a "Max" control to a text control sets the max length
	ESPUI.addControl(Max, "", "32", None, wifi_ssid_text);
	wifi_pass_text = ESPUI.addControl(Text, "Password", "Password", Alizarin, wifi_ssid_text, textCallback);
	ESPUI.addControl(Max, "", "64", None, wifi_pass_text);
	styleSaveWIFIbutton = ESPUI.addControl(Button, "Save & connect", "Save & connect", Sunflower, wifi_ssid_text, enterWifiDetailsCallback);
	ESPUI.setElementStyle(styleSaveWIFIbutton, "border-radius: 2em; border: 3px solid black; width: 30%; background-color: #00ff00;");

	
	/*
	 * Tab: Camera Configuration
	 * You use this tab to configure the camera parameters
	 *-----------------------------------------------------------------------------------------------------------*/
	auto cameraTab = ESPUI.addControl(Tab, "", "Camera Configuration");
	
	//auto grouplabelTEST = ESPUI.addControl(Label, "WiFi","", Sunflower, wifitab);
	pic_interval = ESPUI.addControl(ControlType::Number, "pic interval [s]", String(cam_settings.interval/1000), ControlColor::Alizarin, cameraTab, intervalCallback);
	ESPUI.addControl(Min, "", "0", None, pic_interval);
	ESPUI.addControl(Max, "", "3600", None, pic_interval);
	//interval = ESPUI.addControl(Text, "pic interval","pic interval", Sunflower, cameraTab, intervalCallback);
	//ESPUI.number("Camera interval", intervalCallback, ControlColor::Alizarin, cam_settings.interval, 0, 36000);
	
	//Finally, start up the UI. 
	//This should only be called once we are connected to WiFi.
	ESPUI.begin(HOSTNAME);
}

//Utilities
//------------------------------------------------------------------------------------------------
bool readStringFromEEPROM(char buf[], int baseaddress, int size) {
	int j = 0;
	for (int i = baseaddress; i < baseaddress+size; i++) {
		char c = EEPROM.read(i);
		Serial.print("Read value: ");
		Serial.println(c, HEX);

		if((c == NULL) || (c == 0x00) || (c == '\0') || (c == 0xFF)) break;

		if (isAscii(c))
		{
			buf[j] = c;
			Serial.print(char(buf[j]));
		}
		else 
		{
			Serial.println("Found non-ASCII character in EEPROM - exiting");	
			return false;
		}
		j++;
	}	
	return true;
}

bool readFtpCredentials()
{
  EEPROM.begin(1000);
  if ( not readStringFromEEPROM(ftp_config.ip, EEPROM_PARAM_FTP_SERVER_IP, (EEPROM_PARAM_FTP_SERVER_IP+EEPROM_PARAM_MAX_LENGTH)) || 
  	   not readStringFromEEPROM(ftp_config.username, EEPROM_PARAM_FTP_BENUTZERNAME, (EEPROM_PARAM_FTP_BENUTZERNAME+EEPROM_PARAM_MAX_LENGTH)) ||
	   not readStringFromEEPROM(ftp_config.password, EEPROM_PARAM_FTP_PASSWORT, (EEPROM_PARAM_FTP_PASSWORT+EEPROM_PARAM_MAX_LENGTH)) ||
	   not readStringFromEEPROM(ftp_config.path, EEPROM_PARAM_FTP_PFAD, (EEPROM_PARAM_FTP_PFAD+EEPROM_PARAM_MAX_LENGTH))) 
	   {
			EEPROM.end();
			return false;			
	   }
	   else
	   {
		EEPROM.end();
		return true;
	   }
}

tm getTime( ){
  struct tm timeinfo;
  if(!getLocalTime(&timeinfo)){
    Serial.println("Failed to obtain time");
  }
  else
  {
  return timeinfo;
  }
}


String createTimestamp(struct tm *timeinfo)
{
  String ending = (String(timeinfo->tm_year + 1900) + "_" + String(timeinfo->tm_mon + 1) + "_" + String(timeinfo->tm_mday) + "_" + String(timeinfo->tm_hour) + "_" + String(timeinfo->tm_min) + "_" + String(timeinfo->tm_sec));
  Serial.println(ending);
  return ending;
}

void setup() {
  randomSeed(0);
  Serial.begin(115200);
  Serial.setDebugOutput(true);
  Serial.println();

  connectWifi();
  WiFi.setSleep(false);
  
  setUpUI();

  camera_config_t config;
  config.ledc_channel = LEDC_CHANNEL_0;
  config.ledc_timer = LEDC_TIMER_0;
  config.pin_d0 = Y2_GPIO_NUM;
  config.pin_d1 = Y3_GPIO_NUM;
  config.pin_d2 = Y4_GPIO_NUM;
  config.pin_d3 = Y5_GPIO_NUM;
  config.pin_d4 = Y6_GPIO_NUM;
  config.pin_d5 = Y7_GPIO_NUM;
  config.pin_d6 = Y8_GPIO_NUM;
  config.pin_d7 = Y9_GPIO_NUM;
  config.pin_xclk = XCLK_GPIO_NUM;
  config.pin_pclk = PCLK_GPIO_NUM;
  config.pin_vsync = VSYNC_GPIO_NUM;
  config.pin_href = HREF_GPIO_NUM;
  config.pin_sscb_sda = SIOD_GPIO_NUM;
  config.pin_sscb_scl = SIOC_GPIO_NUM;
  config.pin_pwdn = PWDN_GPIO_NUM;
  config.pin_reset = RESET_GPIO_NUM;
  config.xclk_freq_hz = 20000000;
  config.pixel_format = PIXFORMAT_JPEG;
//  config.frame_size = FRAMESIZE_SVGA;
//  config.frame_size = FRAMESIZE_UXGA;
  config.frame_size = FRAMESIZE_XGA;
  config.jpeg_quality = 12;
  
  // if PSRAM IC present, init with UXGA resolution and higher JPEG quality
  //                      for larger pre-allocated frame buffer.
  if(psramFound()){
    config.frame_size = FRAMESIZE_UXGA;
    config.jpeg_quality = 10;
    config.fb_count = 2;
  } else {
    config.frame_size = FRAMESIZE_SVGA;
    config.jpeg_quality = 12;
    config.fb_count = 1;
  }

#if defined(CAMERA_MODEL_ESP_EYE)
  pinMode(13, INPUT_PULLUP);
  pinMode(14, INPUT_PULLUP);
#endif

  // camera init
  esp_err_t err = esp_camera_init(&config);
  if (err != ESP_OK) {
    Serial.printf("Camera init failed with error 0x%x", err);
    return;
  }

  sensor_t * s = esp_camera_sensor_get();
  // initial sensors are flipped vertically and colors are a bit saturated
  if (s->id.PID == OV3660_PID) {
    s->set_vflip(s, 1); // flip it back
    s->set_brightness(s, 1); // up the brightness just a bit
    s->set_saturation(s, -2); // lower the saturation
  }
  // drop down frame size for higher initial frame rate
  s->set_framesize(s, FRAMESIZE_XGA);

#if defined(CAMERA_MODEL_M5STACK_WIDE) || defined(CAMERA_MODEL_M5STACK_ESP32CAM)
  s->set_vflip(s, 1);
  s->set_hmirror(s, 1);
#endif

/*
//Fall back wifi config, if eeprom credentials are failing
if (WiFi.status() != WL_CONNECTED){
    //WiFi-part
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    Serial.println("");
    Serial.println("WiFi connected");
  }
  */

  // Init and get the time
  configTime(gmtOffset_sec, daylightOffset_sec, ntpServer);
  struct tm timeinfo = getTime( );

  if (&timeinfo != NULL)
  {
    String timestamp = createTimestamp( &timeinfo );
	Serial.println(&timeinfo, "%A, %B %d %Y %H:%M:%S");

  }
  
   if (readFtpCredentials())
   {
		if (WiFi.status() == WL_CONNECTED)
		{
			ESP32_FTPClient ftp (ftp_server,ftp_user,ftp_pass, 5000, 2);
			ftp.OpenConnection();
		}
		else Serial.println("not connected to Wifi, unable connect to FTP server");
   }
   else Serial.println("unable to read ftp-settings from EEPROM");

  startCameraServer();

  Serial.print("Camera Ready! Use 'http://");
  Serial.print(WiFi.localIP());
  Serial.println("' to connect");
}

void writeNewImageToFtp()
{
	ftp.OpenConnection();
	camera_fb_t *buf = getImg();

	// Create the new file and send the image
	ftp.InitFile("Type I");
	//String timeStamp = (&timeinfo, "%Y_");
	struct tm timeinfo = getTime( );

	String timestamp;
	if (&timeinfo != NULL)
	{
	timestamp = createTimestamp( &timeinfo );
	} else 
	timestamp = "NaN";

	String fileName = imgNameBase + timestamp + ".jpeg\0";
	Serial.println(fileName);
	char cFileName [40];
	fileName.toCharArray(cFileName, fileName.length()+1);

	ftp.NewFile(cFileName);
	ftp.WriteData(buf->buf, (sizeof((buf->buf))*(buf->height*buf->width)));
	ftp.CloseFile();
	esp_camera_fb_return(buf);
	imgCnt ++;
}

void loop() {
  if (((millis()-lastImageTaken) > cam_settings.interval) && (WiFi.status() == WL_CONNECTED))
  {
    lastImageTaken = millis();
    writeNewImageToFtp();
  }
}

void reloadFTP(Control *sender, int type) {

	if(type == B_UP) {

		EEPROM.begin(1000);
		String stored_ip, stored_usr, stored_pass, stored_pfad;
		readStringFromEEPROM(ftp_config.ip, EEPROM_PARAM_FTP_SERVER_IP, (EEPROM_PARAM_FTP_SERVER_IP+EEPROM_PARAM_MAX_LENGTH));
		readStringFromEEPROM(ftp_config.username, EEPROM_PARAM_FTP_BENUTZERNAME, (EEPROM_PARAM_FTP_BENUTZERNAME+EEPROM_PARAM_MAX_LENGTH));
		readStringFromEEPROM(ftp_config.password, EEPROM_PARAM_FTP_PASSWORT, (EEPROM_PARAM_FTP_PASSWORT+EEPROM_PARAM_MAX_LENGTH));
		readStringFromEEPROM(ftp_config.path, EEPROM_PARAM_FTP_PFAD, (EEPROM_PARAM_FTP_PFAD+EEPROM_PARAM_MAX_LENGTH));
		EEPROM.end();

		//stored_ip.remove(stored_ip.length()-1);
		ESPUI.updateText(server_IP_text, ftp_config.ip);
		//stored_usr.remove(stored_usr.length()-1);
		ESPUI.updateText(benutzername_text, String(ftp_config.username));
		//stored_pass.remove(stored_pass.length()-1);
		ESPUI.updateText(passwort_text, String(ftp_config.password));
		//stored_pfad.remove(stored_pfad.length()-1);
		ESPUI.updateText(pfad_text,ftp_config.path);
	}
}

void connectWifi() {
	int connect_timeout;

#if defined(ESP32)
	WiFi.setHostname(HOSTNAME);
#else
	WiFi.hostname(HOSTNAME);
#endif
	Serial.println("Begin wifi...");
	//Load credentials from EEPROM 
	if(!(FORCE_USE_HOTSPOT)) {
		yield();
		EEPROM.begin(1000);
		String stored_ssid, stored_pass;

		readStringFromEEPROM(wifi_credentials.ssid, EEPROM_PARAM_WIFI_SSID, (EEPROM_PARAM_WIFI_SSID+EEPROM_PARAM_MAX_LENGTH));
		readStringFromEEPROM(wifi_credentials.password, EEPROM_PARAM_WIFI_PASSWORT, (EEPROM_PARAM_WIFI_PASSWORT+EEPROM_PARAM_MAX_LENGTH));
		EEPROM.end();
		Serial.print("stored ssid: ");
		Serial.println(wifi_credentials.ssid);

		Serial.print("stored password: ");
		Serial.println(wifi_credentials.password);

		stored_ssid.remove(stored_ssid.length()-1);
		ESPUI.updateText(wifi_ssid_text, String(wifi_credentials.ssid));
		ESPUI.updateText(wifi_pass_text, String("***************"));

		//Try to connect with stored credentials, fire up an access point if they don't work.
		#if defined(ESP32)
			WiFi.begin(wifi_credentials.ssid, wifi_credentials.password);
		#else
			WiFi.begin(stored_ssid, stored_pass);
		#endif
		connect_timeout = 28; //7 seconds
		while (WiFi.status() != WL_CONNECTED && connect_timeout > 0) {
			delay(250);
			Serial.print(".");
			connect_timeout--;
		}
	}
	
	if (WiFi.status() == WL_CONNECTED) {
		Serial.println(WiFi.localIP());
		Serial.println("Wifi started");

		if (!MDNS.begin(HOSTNAME)) {
			Serial.println("Error setting up MDNS responder!");
		}
	} else {
		Serial.println("\nCreating access point...");
		WiFi.mode(WIFI_AP);
		WiFi.softAPConfig(IPAddress(192, 168, 1, 1), IPAddress(192, 168, 1, 1), IPAddress(255, 255, 255, 0));
		WiFi.softAP(HOSTNAME);

		connect_timeout = 20;
		do {
			delay(250);
			Serial.print(",");
			connect_timeout--;
		} while(connect_timeout);
	}
}

void enterWifiDetailsCallback(Control *sender, int type) {
	if(type == B_UP) {
		Serial.println("Saving Wifi credentials to EPROM...");
		Serial.println(ESPUI.getControl(wifi_ssid_text)->value);
		Serial.println(ESPUI.getControl(wifi_pass_text)->value);
		unsigned int i;
		EEPROM.begin(1000);
		for(i = 0; i < ESPUI.getControl(wifi_ssid_text)->value.length(); i++) {
			EEPROM.write(i + EEPROM_PARAM_WIFI_SSID, ESPUI.getControl(wifi_ssid_text)->value.charAt(i));
			if(i==EEPROM_PARAM_MAX_LENGTH) break; //Even though we provided a max length, user input should never be trusted
		}
		EEPROM.write(i, '\0');

		for(i = 0; i < ESPUI.getControl(wifi_pass_text)->value.length(); i++) {
			EEPROM.write(i + EEPROM_PARAM_WIFI_PASSWORT, ESPUI.getControl(wifi_pass_text)->value.charAt(i));
			Serial.println(i + EEPROM_PARAM_WIFI_PASSWORT);
			if(i==EEPROM_PARAM_MAX_LENGTH) break;
		}
		EEPROM.write(i + EEPROM_PARAM_WIFI_PASSWORT, '\0');
		EEPROM.end();
		connectWifi();
	}
}

void intervalCallback(Control *sender, int type) {
	cam_settings.interval = sender->value.toInt()*1000;
	Serial.print("interval changed to: ");
	Serial.print(cam_settings.interval);
}

void enterFTPDetailsCallback(Control *sender, int type) {
	if(type == B_UP) {
		Serial.println("Saving FTP credentials to EPROM...");
		Serial.println(ESPUI.getControl(server_IP_text)->value);
		Serial.println(ESPUI.getControl(benutzername_text)->value);
		Serial.println(ESPUI.getControl(passwort_text)->value);
		Serial.println(ESPUI.getControl(pfad_text)->value);
		unsigned int i;
		EEPROM.begin(1000);

		for(i = 0; i < ESPUI.getControl(server_IP_text)->value.length(); i++) {
			EEPROM.write(i + EEPROM_PARAM_FTP_SERVER_IP, ESPUI.getControl(server_IP_text)->value.charAt(i));
			if(i==EEPROM_PARAM_MAX_LENGTH) break;
		}
		EEPROM.write(i + EEPROM_PARAM_FTP_SERVER_IP, '\0');

		for(i = 0; i < ESPUI.getControl(benutzername_text)->value.length(); i++) {
			EEPROM.write(i + EEPROM_PARAM_FTP_BENUTZERNAME, ESPUI.getControl(benutzername_text)->value.charAt(i));
			if(i==EEPROM_PARAM_MAX_LENGTH) break;
		}
		EEPROM.write(i + EEPROM_PARAM_FTP_BENUTZERNAME, '\0');

		for(i = 0; i < ESPUI.getControl(passwort_text)->value.length(); i++) {
			EEPROM.write(i + EEPROM_PARAM_FTP_PASSWORT, ESPUI.getControl(passwort_text)->value.charAt(i));
			if(i==EEPROM_PARAM_MAX_LENGTH) break;
		}
		EEPROM.write(i + EEPROM_PARAM_FTP_PASSWORT, '\0');

		for(i = 0; i < ESPUI.getControl(pfad_text)->value.length(); i++) {
			EEPROM.write(i + EEPROM_PARAM_FTP_PFAD, ESPUI.getControl(pfad_text)->value.charAt(i));
			if(i==EEPROM_PARAM_MAX_LENGTH) break;
		}
		EEPROM.write(i + EEPROM_PARAM_FTP_PFAD, '\0');
		EEPROM.end();
	}
}

void textCallback(Control *sender, int type) {
	//This callback is needed to handle the changed values, even though it doesn't do anything itself.
}