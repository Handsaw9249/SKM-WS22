#include <Arduino.h>
#include <CircularBuffer.h>
#include <complex.h>

// Connect the MEMS AUD output to the ESP32 pins
#define mic1 34
#define mic2 35
#define mic3 36

#define samplingTime 100  // time to push data to the buffer [ms]

// Define audio and setting parameters
#define variance 5        // absolute variance which is allowed as a variance between 2 values
#define check_time 2000   // time to check, whether a person is alive and breathing
#define offset 1800       // define standard level (offset) of microphones       
#define audio_lvl 5       // define allowed difference between 2 related values of both microphones
#define threshold 90      // define an audio threshold which needs to be exceeded to detect a noise

#define lim_silence1 30   // limits for silences
#define lim_silence2 34.5

#define lim_snore1 38     // limits for snoring 
#define lim_snore2 40.5

#define lim_noise 48       // limit for noises, e.g. watching tv, speaking, noises from tradesman etc.

// definition of ratios between the counted values in the histogram [dB]
// and the amount of possible elements (320)
#define snoring_ratio 0.1
#define noise_ratio 0.1
#define silence_ratio 0.1


using namespace std;

// definition of arrays for the fft values
float fft_out1_real[32];
float fft_out1_imag[32];
float fft_out2_real[32];
float fft_out2_imag[32];
float fft_out3_real[32];
float fft_out3_imag[32];

// definition of arrays for the logarithmic amplitudes [dB] for a time period of 32 seconds
float ampl_val1[320];
float ampl_val2[320];
float ampl_val3[320];

// set the time depending on sampling frequency to process a greater interval of information
const int time_multiple_sec = 32*samplingTime;

// buffer definition (for 3.2 s with a sampling time of 100 ms)
CircularBuffer<int,32> micBuf1;       // buffer with 32 elements
CircularBuffer<int,32> micBuf2;       // buffer with 32 elements
CircularBuffer<int,32> micBuf3;       // buffer with 32 elements

// Variables to hold analog values from mic
int micOut1;
int micOut2;
int micOut3;

// timer definition
unsigned long time1 = 0;
unsigned long time2 = 0;
unsigned long time3 = 0;

// set the averages, mamxima and indices of the maxima to zero
float avg1 = 0.0;
float avg2 = 0.0;
float avg3 = 0.0;

float max1 = 0.0;
float max2 = 0.0;
float max3 = 0.0;

int idxMx1 = 0;
int idxMx2 = 0;
int idxMx3 = 0;

// definition of counter and histogram variables
int histo_cnt = 0;
int silence_cnt = 0;
int snoring_cnt = 0;
int noise_cnt = 0;

// defines a variable that 2 microphone values come from the midpoint between both microphones
bool match = false;

// declaration for buffer loops
using index_t1 = decltype(micBuf1)::index_t;
using index_t2 = decltype(micBuf1)::index_t;
using index_t3 = decltype(micBuf1)::index_t;

unsigned long startTime = millis(); // get the start time

// method procBuff1, procBuff2 and procBuff3 serve the same purpose for all three buffers:
// computing the average of every buffer, determining the buffer's maximum value and the associated indice
// plus the fft for every value
// if necessary, the time stamp can be printed too
void procBuff1()
{
  avg1 = 0;
  max1 = 0;
  idxMx1 = 0;
  for (index_t1 i = 0; i < micBuf1.size(); i++) 
  {
    avg1 += micBuf1[i] / (float)micBuf1.size();
    unsigned long elapsedTime1 = micros() - startTime;
    if(max1 < micBuf1[i]) 
      {
        max1 = micBuf1[i];
        idxMx1 = i;
      }
      //Serial.print("1; ");
      //Serial.println(elapsedTime1);

    // determine the fft through the cooley-tukey algorithm with the sum of elements in Eulers format
    // with real and imaginary part
    // this procedure is equivalent to the other buffers
    complex<float> sum(0, 0);
    for (index_t1 j = 0; j < micBuf1.size(); j++)
    {
      float angle = 2 * M_PI * i * j / micBuf1.size();
      float real = cos(-angle);
      float img = sin(-angle);
      sum = sum + float(micBuf1[j]) * complex<float>(real,img);
    }
    fft_out1_real[i] = real(sum);
    fft_out1_imag[i] = imag(sum);

    // compute the logarithmic amplitude [dB]
    ampl_val1[i+(histo_cnt*32)] = 20*log10(sqrt((fft_out1_real[i]*fft_out1_real[i]) + (fft_out1_imag[i]*fft_out1_imag[i])));
  }
}

void procBuff2()
{
  avg2 = 0;
  max2 = 0;
  idxMx2 = 0;
  for (index_t2 i = 0; i < micBuf2.size(); i++) 
  {
		avg2 += micBuf2[i] / (float)micBuf2.size();
    unsigned long elapsedTime2 = micros() - startTime;
    if(max2 < micBuf2[i]) 
    {
      max2 = micBuf2[i];
      idxMx2 = i;
    }    
    //Serial.print("2; ");
    //Serial.println(elapsedTime2);
    complex<float> sum(0, 0);
    for (index_t1 j = 0; j < micBuf2.size(); j++)
    {
      float angle = 2 * M_PI * i * j / micBuf2.size();
      float real = cos(-angle);
      float img = sin(-angle);
      sum = sum + float(micBuf2[j]) * complex<float>(real,img);
    }
    fft_out2_real[i] = real(sum);
    fft_out2_imag[i] = imag(sum);
    ampl_val2[i+(histo_cnt*32)] = 20*log10(sqrt(fft_out2_real[i]*fft_out2_real[i] + fft_out2_imag[i]*fft_out2_imag[i]));
	}
}

void procBuff3()
{
  avg3 = 0;
  max3 = 0;
  idxMx3 = 0;
  for (index_t3 i = 0; i < micBuf3.size(); i++) 
  {
		avg3 += micBuf3[i] / (float)micBuf3.size();
    unsigned long elapsedTime3 = micros() - startTime;
    if(max3 < micBuf1[i]) 
    {
      max3 = micBuf3[i];
      idxMx3 = i;
    }
    //Serial.print("3; ");
    //Serial.println(elapsedTime3);
    complex<float> sum(0, 0);
    for (index_t1 j = 0; j < micBuf3.size(); j++)
    {
      float angle = 2 * M_PI * i * j / micBuf3.size();
      float real = cos(-angle);
      float img = sin(-angle);
      sum = sum + float(micBuf3[j]) * complex<float>(real,img);
    }
    fft_out3_real[i] = real(sum);
    fft_out3_imag[i] = imag(sum);
    ampl_val3[i+(histo_cnt*32)] = 20*log10(sqrt(fft_out3_real[i]*fft_out3_real[i] + fft_out3_imag[i]*fft_out3_imag[i]));
  }
}

void fftHisto()
{
  // set the counters to zero, then count which signal is detected
  silence_cnt = 0;
  snoring_cnt = 0;
  noise_cnt = 0;
  // only 2 of 3 microphones are used, because the use of the middle microphone turned out to not be useful
  for (int i = 0; i < 320 ; i++) 
  {
    // silence is defined between 30 and 34.5 dB, values below 30 dB are ignored
    if(lim_silence1 < (ampl_val1[i] || ampl_val3[i]) < lim_silence2 )
    {
      silence_cnt++;
    } 
    // uncomment and change the 2 following lines, if more types of noises shall be detected

    //else if ( (ampl_val1[i] || ampl_val3[i]) < lim_silence1) noise_cnt++;
    //else if (lim_silence2 < (ampl_val1[i] || ampl_val3[i]) < lim_snore1 ) noise_cnt++;

    // snoring is defined between 38 and 40.5 dB, all other values above it and below 48 dB are defined 
    // as unwanted noises
    if(lim_snore1 < (ampl_val1[i] || ampl_val3[i]) < lim_snore2 )
    {
      snoring_cnt++;
    } else if ( lim_snore2 < (ampl_val1[i] || ampl_val3[i]) < lim_noise) noise_cnt++;
  }

  // the ratio of snoring to the recorded values should be greater than 10 % to detect snoring AND
  // the ratio of silences should be smaller than 10 % to exclude that the dormant state is measured
  // if the ratio of silences is greater than 10 % OR the ratio of noises greater than 10 %,
  // a dormant state and unwanted noises are probably detected
  if (((snoring_cnt/320) > snoring_ratio) && ((silence_cnt/320) <= silence_ratio))
  {
    Serial.print(" Noises from patient detected \n");
  }
  if (((noise_cnt/320) > noise_ratio) || ((silence_cnt/320) >= silence_ratio))
  {
    Serial.print(" Disturbances detected \n ");
  }
}

// method to print the raw values with an offset 
void debugRawValues()
{
  for (index_t1 i = 0; i < micBuf1.size(); i++) 
  {   
    Serial.print("1; ");   
    Serial.println(micBuf1[i]);
  }
  // uncomment the following two lines to get the output from the second buffer
  for (index_t2 i = 0; i < micBuf2.size(); i++) 
  {   
    //Serial.print("2; ");   
    //Serial.println(micBuf2[i]);
  }
  for (index_t3 i = 0; i < micBuf3.size(); i++) 
  {   
    Serial.print("3; ");   
    Serial.println(micBuf3[i]);
  }
}

void debugFFT()
{
  // printing the values: first number for buffer, second number:
  // 1 for real, 2 for imaginary part
  for (int i = 0; i < 32 ; i++) 
  {
    // print fft values of buffer 1
    Serial.print("11; ");
    Serial.println(fft_out1_real[i]);
    Serial.print("12; ");
    Serial.println(fft_out1_imag[i]);

    // print fft values of buffer 2
    Serial.print("21; ");
    Serial.println(fft_out2_real[i]);
    Serial.print("22; ");
    Serial.println(fft_out2_imag[i]);

    // print fft values of buffer 3
    Serial.print("31; ");
    Serial.println(fft_out3_real[i]);
    Serial.print("32; ");
    Serial.println(fft_out3_imag[i]);
  }
}

void debugFFThisto()
{
  for (int i = 0; i < 320 ; i++) 
  {
    // print fft values for histogram of buffer 1, 2 and 3
    // first number for buffer, second number for logarithmic amplitude
    Serial.print(" 15; ");
    Serial.println(ampl_val1[i]);
    Serial.print(" 25; ");
    Serial.println(ampl_val2[i]);
    Serial.print(" 35; ");
    Serial.println(ampl_val3[i]);
  }
}

void debugInformation()
{
  // print the averages and the maximum of the buffer
  Serial.print("1; ");
  Serial.print(avg1);
  Serial.print(" ; ");
  Serial.print(max1);
  Serial.print(" ; ");
  Serial.println(idxMx1);
  Serial.print("2; ");
  Serial.print(avg2);
  Serial.print(" ; ");
  Serial.print(max2);
  Serial.print(" ; ");
  Serial.println(idxMx2); 
  Serial.print("3; ");
  Serial.print(avg3);
  Serial.print(" ; ");
  Serial.print(max3);
  Serial.print(" ; ");
  Serial.println(idxMx3);  
}

void setup() {
  // set the pinModes
  Serial.begin(115200);
  pinMode(mic1, INPUT);
  pinMode(mic2, INPUT);
  pinMode(mic3, INPUT);

  // clear the buffers
  micBuf1.clear();
  micBuf2.clear();
  micBuf3.clear();
}

void loop() {
  // read from the PINs
  micOut1 = analogRead(mic1);
  micOut2 = analogRead(mic2);
  micOut3 = analogRead(mic3);

  // every 100 ms:
  if (millis() - time1 >= samplingTime) 
  {
    time1 = millis();

    // push the values to the buffer; subtract the offset
    micBuf1.push(micOut1-offset);
    micBuf2.push(micOut2-offset);
    micBuf3.push(micOut3-offset);

    // check whether the buffer is full, then compute the average of every buffer
    // and determine the maximum and its indice
    if(micBuf1.isFull())
    {
      // if the buffer is full, process buffer 1
      procBuff1();
    }
		
    if(micBuf2.isFull())
    {
      // if the buffer is full, process buffer 2
      procBuff2();
    }

    if(micBuf3.isFull())
    {
      // if the buffer is full, process buffer 2
      procBuff3();
    }
  }

  // directional characteristic
  // within 2 seconds, check whether the patient gives any (audio) sign of life from the mid point 
  // of 2 microphones
  if (millis() - time2 >= check_time) 
  {
    time2 = millis(); // Reset the timer

    match = false;
    bool lifeSign = false;
    // check whether 2 values from the microphone come from the same direction, through comparing
    // their audio level
    for (index_t1 i = 0; i < micBuf1.size(); i++) 
    { 
      if((abs(micBuf1[i] - micBuf3[i]) + variance) <= audio_lvl)
        {
          // noise was identified incoming from the middle
          match = true;
        }
      // check whether 2 values exceed the threshold value (e.g. snnoring)
      if (match && micBuf1[i] >= threshold  && micBuf3[i] >= threshold)
      {
        lifeSign = true;
      }                     
    }
    if (lifeSign) Serial.print("Sound from patient detected! \n");
    else Serial.print("NOTHING from patient detected! \n");
  }

  // if necessary, print the raw values in the terminal
  // for micros: micros() - time3 >= debugTime
  if (millis() - time3 >= time_multiple_sec)
  {
    time3 = millis(); // Reset the timer

    // uncomment one of the following methods to print rather the 
    // raw values or information about the average, maximum and its index
    // or the fft values or the fft values for a histogram

    //debugRawValues();
    //debugFFT(); 
    //debugInformation();

    // checks whether the counter is greater 10 -> equals 32 seconds
    if (histo_cnt >= 10) // 
    {
      // uncomment this method to print the log. amplitude values of a period of 32 seconds
      //debugFFThisto();

      // reset the counter and determine the frequency of snorings, noises and silences 
      // through the method fftHisto();
      fftHisto();
      histo_cnt = 0;     
    }
    else histo_cnt++;
  }
}
