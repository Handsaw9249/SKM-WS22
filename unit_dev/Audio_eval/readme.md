# Beamforming/Audio Module

## Systemaufbau

- 3 Audiomodule, die als Mikrofon fungieren (ICS-40180)
- Abstand zwischen den Mikrofonen: 20 cm
- Abstand zum Patient: ca. 1 m
![](https://codi.ide3.de/uploads/upload_5eb4885300080cf7e544abfc58a05dcc.jpg)

## Theoretische Betrachtung

- Schall breitet sich mit 343.2 m/s aus, bei Abstand von 20 bzw. 40 cm ergibt sich Zeitunterschied delta t bei Empfangen des gleichen Geräusches bei zwei bzw. drei Mikrofonen
- Delta t liegt im Bereich von ca. 50-500 ys, je nachdem wo sich Patient im Bett befindet
- Beamforming muss mit Abtastfrequenz des ESP32 so bestimmt werden, dass Betrachtung von Werten im gleichen zeitlichen Abstand einen Gesamtwert ergeben

## Realisierung/Implementierung

- 2/3 Mikrofonen werden benutzt, beide äußeren im Abstand von 40 cm (mittlerstes hat sich als nicht sinnvoll zur Nutzung herausgestellt)
- Ringbuffer mit 32 Elementen, in die jede 100 ms ein Rohwert gepusht wird
- Zeitspanne zur Lebenszeichenüberwachung: 3.2 s
- Mikrofone betrachten beide den gleichen Wert im Buffer (Annahme: Patient liegt mittig, Schall trifft gleichzeitig auf beide Sensoren auf), der in der gleichen Größenordnung liegen muss
- Wenn Mikrofone beide auf die gleichen Werte achten, muss dieser einen Schwellwert überschreiten, z. B. 75 (nach Abziehen eines Offsets von 1800) 

## Problematiken

- Alle 50-60 ys kann zwar ein Wert der Mikrofone erfasst werden, deren Größe lassen sich aber nicht vom Grundrauschen des Moduls differenzieren (siehe folgende Grafik)
![](https://codi.ide3.de/uploads/upload_dfe4c6a1f5b02358e8df1b140b562786.png)
- Ein berechneter Zeitunterschied in der Ausbreitung der Schallgeschwindigkeit zwischen 2 Mikrofonen von ca. 50-500 ys wäre zwar in der Verschiebung der Datensätze differenzierbar, jedoch lässt sich generell ein Geräusch, speziell auch das Schnarchen, innerhalb der hohen Abtastraten nicht vom Grundrauschen unterscheiden 
- Grundrauschen bzw. Störgeräusche befinden sich in gleicher Größenordnung wie Schnarchen, sodass Rohwert-analyse nur schwer möglich ist
- Klänge wie z.B. Klatschen oder direktes Sprechen in das Mikrofon wären prinzipiell von anderen Ereignissen zu unterscheiden (siehe folgende Grafik)
![](https://codi.ide3.de/uploads/upload_ce6d979d236164d0222cf7c7d15e6c7b.png)

## Frequenzanalyse

- Auf alle Werten aller Mikrofone wird eine FFT angewendet (Zeitraum 3.2 s), da eine Rohwert-analyse allein nicht zielführend ist
- Über einen Zeitraum von 32 s wird die Häufigkeit des Schnarchens und von Störgeräuschen gezählt
- Festlegung von Verhältnissen von Schnarchen/Ruhezuständen definiert den Zustand des Schlafens bzw. des Schnarchens eines Patienten
- Verhältnisse und Grenzwerte für Geräuschpegel [dB] müssen im Nachhinein optimiert werden und analysiert werden, Programm liefert lediglich Realisierung von Grundfunktionalitäten, die z. B. mit KI oder mit Langzeitanalysen verbessert werden müssen

## Ergebnis

- Programm kann grundlegend auf ein mittig von 2 Mikrofonen eingehendes Geräusch reagieren, jedoch müssen die Grenzwerte für die Erkennung dieses Geräuschs weiter optimiert werden
- Klassisches "Beamforming" für Erkennung des Schnarchens in dieser Konstellation nicht möglich; Vergrößern des Abstandes oder Verwendung sensiblerer Mikrofone könnte Abhilfe schaffen
- Programm führt grobe Frequenzanalyse durch, sodass mit Langzeitanalyse oder KI ein Schnarchen bzw. Lebenszeichen eines Patienten detektiert werden können 