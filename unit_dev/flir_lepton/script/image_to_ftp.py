#!/usr/bin/env python3
#
# Simple tCam display program using matplotlib and TCam
#
#  Note: Useful palette values are 'twilight', 'twilight_shifted', 'hsv', 'ocean',
#   'gist_earth', 'terrain', 'gist_stern', 'gnuplot', 'gnuplot2', 'CMRmap', 'cubehelix'
#   'brg', 'gist_rainbow', 'rainbow', 'jet', 'turbo', 'nipy_spectral', 'gist_ncar'
#

import argparse
import array
import base64
import matplotlib.pyplot as plt
import matplotlib.image as mpimg
import numpy as np
import sys
from tcam import TCam
from PIL import Image
import ftplib
import io
import datetime;
import time

session = ftplib.FTP('0.0.0.0','ftpuser','123456')

interval = 30
timeLastPush = time.time()

if __name__ == "__main__":

    ip = "192.168.4.1"
    palette = "CMRmap"

    #
    # Open camera
    #
    cam = TCam()
    stat = cam.connect(ip)

    while 1:
        if ((time.time()-timeLastPush) > interval):

            if stat["status"] != "connected":
                print(f"Could not connect to {ip}")
                cam.shutdown()
                sys.exit()

            #
            # Get image and decode radiometric data into an array of uint16 values
            #
            try: 
                img_json = cam.get_image()
                dec_rad = base64.b64decode(img_json["radiometric"])
                ra = array.array('H', dec_rad)

                #
                # Determine minimum/maximum 16-bit values in radiometric data
                #
                imgmin = 65535
                imgmax = 0

                for i in ra:
                    if i < imgmin:
                        imgmin = i
                    if i > imgmax:
                        imgmax = i

                delta = imgmax - imgmin
                print(f"Max val is {imgmax}, Min val is {imgmin}, Delta is {delta}")

                #
                # Linearize 16-bit data within range imgmin/imgmax to an 8-bit image
                #
                a = np.zeros((120, 160, 3), np.uint8)
                for r in range(0, 120):
                    for c in range(0, 160):
                        val = int((ra[(r * 160) + c] - imgmin) * 255 / delta)
                        if val > 255:
                            a[r, c] = [255, 255, 255]
                        else:
                            a[r, c] = [val, val, val]

                #
                # Slice into a single-color image so we can colorize it using a palette
                #
                lum_img = a[:, :, 0]
                session.set_pasv(True)

                #print(a)
                im = Image.fromarray(a)
                print(im)
                buffer = io.BytesIO()
                im.save(buffer, format="JPEG")

                # ct stores current time
                ct = datetime.datetime.now()
                print("current time:-", ct)
                filename = "espCamThermalImg_" + str(ct.year) + "_" + str(ct.month) + "_" + str(ct.day) + "_" + str(ct.hour) + "_" + str(ct.minute) + "_" + str(ct.second) + ".jpeg"

                buffer.seek(0)
                session.storbinary(f"STOR {filename}", buffer)

                #session.quit()
                #im.save("filename.jpeg")

                #
                # Display
                #  Note: supported interpolation values are 'antialiased', 'none', 'nearest', 'bilinear',
                #   'bicubic', 'spline16', 'spline36', 'hanning', 'hamming', 'hermite', 'kaiser',
                #   'quadric', 'catrom', 'gaussian', 'bessel', 'mitchell', 'sinc', 'lanczos', 'blackman'
                #
                # plt.imshow(lum_img, interpolation="antialiased", cmap=palette)
                # plt.colorbar()
                # plt.show()

                #cam.shutdown()

                timeLastPush = time.time()
            except:
                print("could not get new image")

