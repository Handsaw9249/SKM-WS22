# Wärmebildkamera

Zur Erkennung von Lebenszeichen und/oder der Atemfrequenz kann eine Wärmebildkamera verwendet werden. Während des Atemvorgangs kühlen sich die Bereiche um die Nasenlöcher bzw. den Mund signifikant im Vergeich zum Ausatmen ab.

Einatmen:
![](graphics/breath_in.png)

Ausatmen:
![](graphics/breath_out.png)
Um eine Erkennung auf Basis von Einzelbildern durchführen zu können, müssen die Fotos in einer im Vergleich zur maximalen Atemfrequenz mehr als doppelt so großen Frequenz aufgenommen werden ([Nyquist-Frequenz](https://de.wikipedia.org/wiki/Nyquist-Frequenz), [Aliasing](https://de.wikipedia.org/wiki/Alias-Effekt)).

Auf Basis dieser Bilder ist es nun möglich mit Hilfe von Bildanalyseverfahren die pulsierenden Bereiche zu erkennen und festzustellen, ob es sich bei diesen Stellen um Mund oder Nase handelt. 
Anschließend ist es möglich, die durchschnittlichen Temperaturwerte dieser Stellen über die Zeit zu samplen und mit Hilfe einer FFT anschließend die Atemfrequenz zu ermitteln. 

## Hardware

### Sensor

Der Bildsensor ist ein [FLIR Lepton v3.5](https://www.flir.de/products/lepton/?model=500-0771-01&vertical=microcam&segment=oem), der eine Auflösung von 160x120 Pixel und eine Temperaturauflösung von unter 50 mK besesitzt.

![](graphics/FLIR%20Lepton%20v3.5.png)

### Breakout Board

Um die Daten vom Sensor abzufragen wird ein Breakoutboard benötigt, das die Anschlüsse des MOLEX-Sockels des Sensors auf Pins zur Anbindung an einen Microcontroller ermöglicht. Hierzu wird das offizielle  [FLIR Lepton Breakout Board v2.0](https://www.flir.de/products/lepton-breakout-board-v2.0?vertical=microcam&segment=oem) verwerndet.

![](graphics/leptoncamerabreakoutboard-v2-a.png)


### Microcontroller

Zu Verarbeitung bzw. Aufbereitung der Daten wird ein ESP32 verwendet, der eine drahtlose Übertragung der Bilddaten bietet. Aufgrund der großen Bilddaten, wird ein ESP32-Modul benötigt, das einen PSRAM besitzt, der ein schnelles Zwischenspeichern der Daten ermöglicht. Hierzu kann z. B. ein aiThinker ESP32-CAM-Modul verwendet werden. Dabei muss die Kamera jedoch entfernt werden, da diese über den einzigen I2C-Bus des MCUs angebunden ist, der für die Wärmebildkamera benötogt wird.

![](graphics/aiThinker_espcam_marked.png)

### USB-TTL-Adapter
Da das ESP-Cam-Modul keinen USB-Port besitzt, ist ein UART-TTL-Adapter vonnöten, um den ESP32 flashen zu können.

### Schaltung

Gemäß folgender Abbildung sind das Breakoutboard und das ESP32-Modul miteinander zu verbinden. Die I2C-Pins werden nicht auf den Pinheader rausgeführt, da diese direkt mit dem Kameramodul der ESP-Cam verbunden sind. Deshalb muss diese entfernt werden und die I2C-Leitungen, SCL und SDA, müssen direkt an das aufgelötete ESP-Modul gelötet werden. Die benötigten Pins sind in der oben stehenden Grafik des ESP-Cam-Moduls rot markiert.

![](graphics/breakoutboard-flir-lepton.svg)



- [FLIR Lepton v3.5](https://www.flir.de/products/lepton/?model=500-0771-01&vertical=microcam&segment=oem)[Engeneering Datenblatt](https://flir.netx.net/file/asset/13333/original/attachment)
- [FLIR Lepton Breakout Board v2.0](https://www.flir.de/products/lepton-breakout-board-v2.0?vertical=microcam&segment=oem), [Datenblatt](https://flir.netx.net/file/asset/23497/original/attachment)
- [ESP32 mit PSRAM & Rev. 3 (z.B. AI Thinker)](https://docs.ai-thinker.com/en/esp32-cam), [Dokumentation](https://docs.ai-thinker.com/en/esp32-cam)


## Software

### Arduino Framework
Es existieren bereits mehrere Projekte, die versuchen, ein Auslesen der FLIR Lepton-Module zu ermöglichen. Unter Verwendung der am weitesten verbreiteten [Bibliothek](https://github.com/NachtRaveVL/Lepton-FLiR-Arduino), konnten zwar Bildteile empfangen werden, die Übertragung ist jedoch nach wenigen Zeilen Abgebrochen, da die Synchronisation nicht möglich war. Dadurch hat der MCU bei der Übertragung Zeilen Überprungen, wodurch die Daten unbrauchbar wurden und die Übertragung letzten Endes abgebrochen wurde. Dies ist eine Limitierung der aktuellen Version der Bibliothek. Der Entwickler ist sich der Problematik bewusst und plant, die Bibliothek neu zu schreiben. Die neue Version soll auch den VSYNC-Pin des Breakout-Boards nutzen, der zur Synchronisierung der Datenübertragung dient.
Zum Zeitpunkt der Durchführung dieses Projektes steht die neue Version jedoch noch nicht zur Verfügung.

### Espressive Framework
Neben dem Arduino-Framework kann der ESP32 auch mit dem Espressive IDF programmiert werden. Für dieses Framework existiert ein [Projekt](https://github.com/danjulio/tCam), das das Auslesen der Bilddaten aus dem IR-Sensor ermöglicht. Im Rahmen dieses Projektes wurden Platinen entwickelt, die eine Wärmebildkamera in kleinem Formfaktor realisieren.

Als Basis für dieses Projekt wurde das Unterprojekt tCam-Mini verwendet.
Da eine andere Hardware verwendet wird, als in der tCam-Mini vorgesehen, müssen Anpassungen vorgenommen werden und die Firmware anschließend neu kompiliert werden. Eine Verwendung der vorkompilierten Firmware ist nicht möglich.
 
#### Veränderungen der Originalfirmware

Die Originalfirmware ist auf einen ESP32 mit 8MB FLASH ausgelegt. Da das AiThinker ESP-CAM-Modul jedoch nur 4MB besitzt, muss die Partitionierung angepasst werden. Hierzu muss die Datei `partitions.csv` modifiziert werden. Da keine Over-the-Air-Updates benötigt werden, können die beiden 2MB großen Partitionen entfernt werden.

Da eine andere Hardware verwendet wird, als in der tCam-Mini, muss auch das Pinmapping angepasst werden.
Dazu werden Änderungen in der Datei `system_config.h` vorgenommen:

```c
// Wifi/Serial board pins
#define BRD_W_MODE_SENSE_IO     32

#define BRD_W_BTN_IO            0
#define BRD_W_RED_LED_IO        14
#define BRD_W_GREEN_LED_IO      15

#define BRD_W_LEP_SCK_IO        14
#define BRD_W_LEP_CSN_IO        15
#define BRD_W_LEP_VSYNC_IO      13
#define BRD_W_LEP_MISO_IO       12
#define BRD_W_LEP_RESET_IO      -1

#define BRD_W_I2C_MASTER_SDA_IO 21
#define BRD_W_I2C_MASTER_SCL_IO 22

#define BRD_W_SIF_RX_IO         26
#define BRD_W_SIF_TX_IO         27

#define BRD_W_HOST_SCK_IO       34
#define BRD_W_HOST_MISO_IO      33
#define BRD_W_HOST_CSN_IO       35

#define BRD_W_ETH_MDC           -1
#define BRD_W_ETH_MDIO          -1
#define BRD_W_PHY_RESET         -1
```
Die geänderten Defines sind die Pins `BRD_W_LEP_#` und `BRD_W_I2C_MASTER_#`, die entsprechend der Schaltung angepasst werden müssen.

#### Kompilieren der Firmware
Um die das aufgrund diverser Abhängigkeiten komplizierte Aufsetzen des Espressive IDF zu vermeiden, kann ein Docker-Container mit vorinstallierter IDF verwendet werden. Dieser ist im [Docker-Hub](https://hub.docker.com/r/espressif/idf) zu finden. Wichtig ist, dass das Kompilieren mit der gleichen Version erfolgt, mit der das Projekt angelegt wurde, da eine Abwärtskompatibilität **nicht** immer gegeben ist. Zum Zeitpunkt des Projektes befindet sich das die Firmware-Version auf Stand der Espressive IDF v4.4.2. Die aktuellste Version ist im Unterordner `firware` der tCam-Mini im Repository zu [finden](https://github.com/danjulio/tCam/tree/main/tCam-Mini/firmware).
Hierfür wird eine Docker-Installation benötigt. Eine Anleitung dazu ist in der Readme-Datei des unit-dev des [ftp-servers](../ftp_server/Readme.md) zu finden.
Folgender Befehl muss zum Kompilieren im Projektordner (hier `firmware`) ausgeführt werden:

`docker run --device=/dev/ttyUSB1 --rm -v $PWD:/project -w /project -it espressif/idf:v4.4.2`

Bei diesem Befehl sind sowohl der serielle Port des UART-TTL-Adapters zu ersetzen, als auch die Version des IDF, die zum aktuellen Zeitpunkt Version 4.4.2 ist. Bei der ersten Ausführung, wird der Docker-Container zunächst heruntergeladen. Anschließend wird eine Konsole des Containers geöffnet.
Hier kann das Kompilieren mit dem Befehl `idf.py build` gestartet werden.
Nach erfolgter Kompilierung wird das Binary mithilfe des folgenden Kommandos auf den ESP32 geladen:
`/opt/esp/python_env/idf4.4_py3.8_env/bin/python ../opt/esp/idf/components/esptool_py/esptool/esptool.py -p /dev/ttyUSB1 -b 460800 --before default_reset --after hard_reset --chip esp32  write_flash --flash_mode dio --flash_size detect --flash_freq 80m 0x1000 build/bootloader/bootloader.bin 0x8000 build/partition_table/partition-table.bin 0xd000 build/ota_data_initial.bin 0x10000 build/tCamMini.bin`
Auch hier muss der Port des UART-TTL-Adapters angepasst werden. Weiterhin ist wichtig, dass die Flashgröße nicht automatisch detektiert wird (`--flash_size detect`), sondern statisch auf 4MB gesetzt wird (`--flash_size 4MB`).
Um das Board flashen zu können, muss weiterhin ein Ping des ESP32 auf Ground gezogen werden. Diese Verbindung ist in der Schaltung als JP1 gekennzeichnet.

Nach dem Flashen muss der Jumper wieder entfernt werden. Nach einem Reset startet der ESP und stellt einen AP bereit. Mithilfe der [tCam Desktop-App](https://github.com/danjulio/tCam/tree/main/DesktopApp) kann anschließend die Funktionalität überprüft werden. Hierzu muss sich der Rechner mit dem offenen AP der tCam verbinden. Das Gerät erhält eine IP-Adresse im Subnnetz `192.168.4.1/24`, wobei die Kamera die IP-Adresse `192.168.4.1` besitzt. Mit dieser IP-Konfiguration kann die Desktop-App eine Verbindung zur Wärmebildkamera aufbauen. In der App können Bilder aufgenommen werdem, unterschiedliche Visualisierungen ausgewählt werden und Videoaufnahmen angefertigt werden. Weiterhin kann die Kamera in der Desktop App so konfiguriert werden, dass sie sich mit einem bestehen WiFi-Netzwerk verbindet.

### Python-Skript
Zum regelmäßigen Abrufen von Bildern der Kamera wurde ein Python-Skript erstellt, das in einem zu definierenden Intervall Bilder der Kamera abruft und diese im gleichen Verzeichnis wie die Bilder der RGB-ESP-Cam ablegt. Von dort können die mit Zeitstempel versehenen Bilder abgerufen und analysiert werden.
