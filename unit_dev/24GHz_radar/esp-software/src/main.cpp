#include <Arduino.h>

#include <sleepbreathingradar.h>
#include "SoftwareSerial.h"
#include <WiFi.h>
#include <PubSubClient.h>

#define RX_PIN 25
#define TX_PIN 26

SleepBreathingRadar radar;

const char* ssid = "fuchsbau";
const char* password = "50583356519873956870";

const char* mqtt_server = "192.168.178.81";

WiFiClient espClient;
PubSubClient client(espClient);
long lastMsg = 0;
char msg[50];
int value = 0;

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("ESP8266Client")) {
      Serial.println("connected");
      // Subscribe
      client.subscribe("esp32/output");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}


void setup()
{
  radar.SerialInit();
  Serial.begin(115200);
  delay(1500);
  Serial.println("Ready");
  setup_wifi();
  client.setServer(mqtt_server, 1885);
  //client.setCallback(callback);
}

void loop()
{
 radar.recvRadarBytes();                       //Receive radar data and start processing
 if (radar.newData == true) {                  //The data is received and transferred to the new list dataMsg[]
    byte dataMsg[radar.dataLen+1] = {0x00};
    dataMsg[0] = 0x55;                         //Add the header frame as the first element of the array
    for (byte n = 0; n < radar.dataLen; n++)dataMsg[n+1] = radar.Msg[n];  //Frame-by-frame transfer
    radar.newData = false;                     //A complete set of data frames is saved
    


    //radar.ShowData(dataMsg);                 //Serial port prints a set of received data frames
    int situation = radar.Situation_judgment(dataMsg);         //Use radar built-in algorithm to output human motion status

    uint8_t msgByte[radar.dataLen];
    //initialize Message with 0
    for (int i = 0; i <= radar.dataLen; i++){
      msgByte[i] = 0;
    }
    //Serial.println(situation);
    Serial.printf("dataMsg: ");
    for (int i = 0; (i <= sizeof(msgByte)/sizeof(msgByte[0])) && (i <=radar.dataLen); i++)
    {
      msgByte[i] = dataMsg[i];
      Serial.printf("%X ", dataMsg[i]);
    } 
    Serial.printf("\n");

    if (!client.connected()) {
      reconnect();
    }
    // if ( client.connected() && situation >= 0 && situation <=16 )
    // {
    Serial.printf("MQTT Message: ");
    for (int i = 0; i < sizeof(msgByte)/sizeof(msgByte[0]); i++)
    {
      Serial.printf("%X ", msgByte[i]);
    }
    Serial.printf("\n");

    Serial.printf("Sizeof msgByte = %i\n", sizeof(msgByte));
    client.publish("skm/24Ghz", msgByte, sizeof(msgByte), false);

    //}
    radar.Bodysign_judgment(dataMsg, 1, 15);
    radar.Sleep_inf(dataMsg);
  }

  
}
