# importing psycopg2 module
import psycopg2
from datetime import datetime, timezone


# establishing the connection
conn = psycopg2.connect(
	database="postgres",
	user='postgres',
	password='postgres',
	host='192.168.178.73',
	port='5436'
)

# creating a cursor object
cursor = conn.cursor()

# creating table
sql = '''CREATE TABLE employee(
id SERIAL NOT NULL,
name varchar(20) not null,
state varchar(20) not null
)'''


# list that contain records to be inserted into table
data = [('Babita', 'Bihar'), ('Anushka', 'Hyderabad'),
		('Anamika', 'Banglore'), ('Sanaya', 'Pune'),
		('Radha', 'Chandigarh')]

# inserting record into employee table
dt = datetime.now(timezone.utc)

for d in data:

	cursor.execute("INSERT into employee(time, name, state, uuid) VALUES (%s, %s, %s, (SELECT uuid_generate_v4()))", (dt, d[0], d[1]))


print("List has been inserted to employee table successfully...")

# Commit your changes in the database
conn.commit()

# Closing the connection
conn.close()
