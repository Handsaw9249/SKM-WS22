# python 3.6

import random
import time
import psycopg2
from datetime import datetime, timezone

from paho.mqtt import client as mqtt_client

SLEEP_INF = 0x05

SLEEP_INF = 0x05          #Sleep radar data header frames
BREATH = 0x01             #Breathing parameters
SCENARIO = 0x03           #Scenario evaluation
SLEEP_TIME = 0x04         #Duration parameters
SLEEP_QUALITY = 0x05      #Sleep quality

REATH_RATE = 0x01        #Breathing rate
CHECK_SIGN = 0x04         #Detection signal

BREATH_HOLD = 0x01        #Breath-holding abnormalities
BREATH_NULL = 0x02        #None
BREATH_NORMAL = 0x03      #Normal breathing
BREATH_MOVE = 0x04        #Movement abnormalities
BREATH_RAPID = 0x05       #Acute respiratory abnormalities

CLOSE_AWAY_BED = 0x07     #Getting in and out of bed judgment
SLEEP_STATE = 0x08        #Sleep state judgment

BREATH_RATE = 0x01        #Breathing rate
CHECK_SIGN = 0x04         #Detection signal

AWAY_BED = 0x00           #Leaving the bed
CLOSE_BED = 0x01          #Get into bed

AWAKE = 0x00              #sleep state:Awake
LIGHT_SLEEP = 0x01        #sleep state:Light sleep
DEEP_SLEEP = 0x02         #sleep state:deep sleep
SLEEP_NULL = 0x03         #no sleep state

AWAKE_TIME = 0x01         #Awake time
LIGHT_SLEEP_TIME = 0x02   #Light sleep time
DEEP_SLEEP_TIME = 0x03    #Deep sleep time

SLEEP_SCORE = 0x01        #Sleep quality score


broker = '0.0.0.0'
port = 1885
topic = "skm/24Ghz"
# generate client ID with pub prefix randomly
client_id = f'python-mqtt-{random.randint(0, 1000)}'

def connect_mqtt():
    def on_connect(client, userdata, flags, rc):
        if rc == 0:
            print("Connected to MQTT Broker!")
        else:
            print("Failed to connect, return code %d\n", rc)

    client = mqtt_client.Client(client_id)
    #client.username_pw_set(username, password)
    client.on_connect = on_connect
    client.connect(broker, port)
    return client


def publish(client):
    msg_count = 0
    while True:
        time.sleep(1)
        msg = f"messages: {msg_count}"
        result = client.publish(topic, msg)
        # result: [0, 1]
        status = result[0]
        if status == 0:
            print(f"Send `{msg}` to topic `{topic}`")
        else:
            print(f"Failed to send message to topic {topic}")
        msg_count += 1

def writeIntoDB(conn, cursor, msg):
    # create timestamp
    dt = datetime.now(timezone.utc)

    print("msg length: " + str(len(msg.payload)))
    sleepInformation = None
    if (len(msg.payload) >= 8):
        if(msg.payload[3] == SLEEP_INF):
            if (msg.payload[4] == BREATH):
                if (msg.payload[5] == BREATH_RATE):
                    print("Breathing rate is: " + str(msg.payload[6]))
                    sleepInformation = "Breathing rate is: " + str(msg.payload[6])
                    cursor.execute("INSERT into breathing(uuid, timestamp, breathingrate) VALUES ((SELECT uuid_generate_v4()), %s, %s)", (dt, msg.payload[6]))
                elif (msg.payload[5] == CHECK_SIGN):
                    if (msg.payload[6] == BREATH_HOLD):
                        print("Abnormal breath-holding detected.")
                        sleepInformation = "Abnormal breath-holding detected."
                    elif (msg.payload[6] == BREATH_NULL):
                        print("No detection signal at the moment.")
                        sleepInformation = "No detection signal at the moment."
                    elif (msg.payload[6] == BREATH_NORMAL):
                        print("Normal breathing was detected.")
                        sleepInformation = "Normal breathing was detected."
                    elif (msg.payload[6] == BREATH_MOVE):
                        print("Abnormal motion is detected.")
                        sleepInformation = "Abnormal motion is detected."
                    elif (msg.payload[6] == BREATH_RAPID):
                        print("Abnormal shortness of breath was detected.")
                        sleepInformation = "Abnormal shortness of breath was detected."
                elif (msg.payload[4] == SCENARIO):
                    if (msg.payload[5] == CLOSE_AWAY_BED):
                        if(msg.payload[5] == AWAY_BED):
                            print("Detects someone leaving the bed.")
                            sleepInformation = "Detects someone leaving the bed."
                        elif(msg.payload[5] == CLOSE_BED):
                            print("Detects someone in bed.")      
                            sleepInformation = "Detects someone in bed."
                    elif (msg.payload[5] == SLEEP_STATE):
                        if(msg.payload[5] == AWAKE):
                            print("Current user status detected: Awake.")
                            sleepInformation = "Current user status detected: Awake."
                        elif(msg.payload[5] == LIGHT_SLEEP):
                            print("Current user status detected: Light sleep.")
                            sleepInformation = "Current user status detected: Awake."
                        elif(msg.payload[5] == DEEP_SLEEP):
                            print("Current user status detected: Deep sleep.")
                            sleepInformation = "Current user status detected: Deep sleep."
                        elif(msg.payload[5] == SLEEP_NULL):
                            print("Current user status detected: NULL.")
                            sleepInformation = "Current user status detected: NULL."
                elif(msg.payload[4] == SLEEP_TIME):
                    if (msg.payload[5] == AWAKE_TIME):
                        print("The user's awake time is detected as: " + str(SleepTimeCalculate(msg.payload[6], msg.payload[7], msg.payload[8], msg.payload[9])))
                        sleepInformation = "The user's awake time is detected as: " + str(SleepTimeCalculate(msg.payload[6], msg.payload[7], msg.payload[8], msg.payload[9]))
                    elif(msg.payload[5] == LIGHT_SLEEP_TIME):
                        print("The user's light sleep time is detected as: " + str(SleepTimeCalculate(msg.payload[6], msg.payload[7], msg.payload[8], msg.payload[9])))
                        sleepInformation = "The user's light sleep time is detected as: " + str(SleepTimeCalculate(msg.payload[6], msg.payload[7], msg.payload[8], msg.payload[9]))
                    elif(msg.payload[5] == DEEP_SLEEP_TIME):
                        print("The user's deep sleep time is detected as: " + str(SleepTimeCalculate(msg.payload[6], msg.payload[7], msg.payload[8], msg.payload[9])))
                        sleepInformation = "The user's deep sleep time is detected as: " + str(SleepTimeCalculate(msg.payload[6], msg.payload[7], msg.payload[8], msg.payload[9]))
                    time = SleepTimeCalculate(msg.payload[6], msg.payload[7], msg.payload[8], msg.payload[9])
                elif(msg.payload[4] == SLEEP_QUALITY):
                    if (msg.payload[5] == SLEEP_SCORE):
                        print("Judgment of sleep quality scores" + str(msg.payload[6]))
                        sleepInformation = "Judgment of sleep quality scores" + str(msg.payload[6])
    if sleepInformation != None:
        cursor.execute("INSERT into sleepinformation(uuid, timestamp, information) VALUES ((SELECT uuid_generate_v4()), %s, %s)", (dt, sleepInformation))
    cursor.execute("INSERT into rawdata(uuid, timestamp, rawdata) VALUES ((SELECT uuid_generate_v4()), %s, %s)", (dt, msg.payload))
    conn.commit()

def SleepTimeCalculate(inf1, inf2, inf3, inf4):
  rel = 0
  rel = (inf1 <<  24)+(inf2 << 16 )+(inf3 << 8)+(inf4)
  print(rel)
  return rel

def subscribe(client: mqtt_client, cursor, conn):
    def on_message(client, userdata, msg):
        #print(msg.payload.type())
        print(f"Received `{msg.payload}` from `{msg.topic}` topic")
        writeIntoDB(conn, cursor, msg)

    client.subscribe(topic)
    client.on_message = on_message

def connToDatabase():
    # establishing the connection
    conn = psycopg2.connect(
        database="postgres",
        user='postgres',
        password='postgres',
        host='192.168.178.81',
        port='5436'
    )
    # creating a cursor object
    return conn

def run():
    client = connect_mqtt()
    conn = connToDatabase()
    cursor = conn.cursor()
    #publish(client)
    subscribe(client, cursor, conn)
    client.loop_forever()

if __name__ == '__main__':
    run()
