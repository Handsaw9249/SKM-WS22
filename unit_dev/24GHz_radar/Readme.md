# 24GHz Radar

## Hardware
Die Firma [Seedstudio](https://www.seeedstudio.com) hat Anfang 2021 mehrere Radar-Module vorgestellt, die auf Basis von Infineon FMCW (Frequency Modulated Continuous Wave) Radaren, Auswertungen vornehmen. 
Drei der Radare arbeiten mit 24GHz und eins mit 60GHz, was laut Seedstudio zusätzlich die Möglichkeit des Detektierens des Herzschlags ermöglichen soll.

| Produkt | Preis | Features |
|---|---|---|
| [24GHz Human Static Presence Lite](https://www.seeedstudio.com/24GHz-mmWave-Sensor-Human-Static-Presence-Module-Lite-p-5524.html)  | 6,90$  |  Erkennung menschlicher Anwesenheit |
| [24GHz Human Stationary Sensor](https://www.seeedstudio.com/24GHz-mmWave-Radar-Sensor-Human-Static-Presence-Module-p-5267.html) | 19,90$ | Bewegungserkennung, Anwesenheit, auch stationär, Atemfrequenz, Sturzerkennung |
| [24GHz Respirational Sleep Sensor](https://www.seeedstudio.com/24GHz-mmWave-Radar-Sensor-Sleep-Breathing-Monitoring-Module-p-5304.html) | 28,00$ | Bewegungserkennung, Anwesenheit, auch stationär, Atemfrequenz, Schlaferkennung |
| [60GHz Resporatory Heartbeat Sensor](https://www.seeedstudio.com/60GHz-mmWave-Radar-Sensor-Breathing-and-Heartbeat-Module-p-5305.html) | 45,00$ | Bewegungserkennung, Anwesenheit, auch stationär, Atemfrequenz, Schlaferkennung mit Langzeitdaten (Schlafposition), Herzschlagfrequenzerkennung |
| [60GHz Fall Detection Pro Sensor](https://www.seeedstudio.com/60GHz-mmWave-Radar-Sensor-Fall-Detection-Module-Pro-p-5375.html) | 37,00$ | Bewegungserkennung, Anwesenheit, auch stationär, Sturzerkennung |

Die Radar-Module enthalten bereits einen Microcontroller mit einer Software, die die Auswertung der Radardaten vornimmt. Es ist nicht möglich, an die Rohdaten (IQ-Daten) des Radars auszulesen. Die Algorithmen zur Auswertung sind nicht open source und die Firmware ist bis jetzt nicht modifizierbar. 
Sobald das Radar ein Ereigis detektiert, werden die Informationen über UART ausgegeben. Weiterhin haben die Module noch digitale Ausgänge, über die rudimentäre Informationen (wie z.B. ob deine menschliche Anwesenheit detektiert wurde) ausgegenben.

## Software
Die Software besteht aus drei Teilen.
- Closed source Radarauswertung
- ESP32 Firmware 
- Server Backend
    - Datenbank
    - MQTT-Server
    - Python-Auswertung

Der ESP32 wertet die über UART erhaltenen Daten aus und sendet die vom Radar erhaltenen Datenpakete über MQTT. Ein auf dem Server laufendes Python-Skript empfängt die Daten und wertet diese aus. Die Rohdaten und die dekodierten Informationen werden mit einer UUID und einem Zeitstempel in eine auf dem Server laufende Postgres-Datenbank geschrieben. Die Serverdienste (MQTT-Broker und Datenbank) werden über einen Docker-compose-Stack ausgeführt. Eine Anleitung zur Ausführung befindet sich in [dieser Dokumentation](../ftp_server/Readme.md). 
Die Ausführung des Stacks im Hintergrund erfolgt über das Kommando `docker compose up -d`. Hierfür muss das aktuelle Arbeitsverzeichnis der Ordner `docker-compose` sein. Das Pythonskript wird im Ordner `python-mqtt` nach Aktivierung der virtuellen Pythonumgebung durch `. bin/activate` mit dem Befehl `python3 main.py` gestartet.
Das Skript schreibt die Rohdaten in die Datenbank und wertet die Datenpakete, die Informationen bezüglich des Schlafes und der Atmung enthalten, aus und schreibt die Ereignisse in Tabellen. So ist auch im Nachhinein noch eine Auswertung der Protokolle möglich, die aktuell noch nicht dekodiert werden.

# Einschätzung
Die Auswertung der vom Radar erhaltenen Daten hat gezeigt, dass die Radarmodule mehr versprechen als sie halten können. Die Daten können jedoch als Anhaltspunkt dienen. Eine Detektion der Atmung kann im Bereich zwischen ca. 0,16Hz und 2,4Hz detektiert werden. In ruhiger Position lässt sich diese 